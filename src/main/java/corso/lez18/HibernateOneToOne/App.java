package corso.lez18.HibernateOneToOne;

import org.hibernate.Session;

import corso.lez18.HibernateOneToOne.models.Dipendente;
import corso.lez18.HibernateOneToOne.models.Indirizzo;
import corso.lez18.HibernateOneToOne.models.db.GestoreSessioni;

public class App 
{
    public static void main( String[] args )
    {

    	Session sessione =  GestoreSessioni.getIstanza().getFactory().getCurrentSession();
    	
    	Indirizzo ind = new Indirizzo("Via le mani dal naso", "Roma", "Roma");
    	
    	Dipendente giovanni = new Dipendente("DIP1234", "Giovanni Pace");
    	giovanni.setIndirizzo(ind);
    	
    	try {
			
    		sessione.beginTransaction();
    		
    		sessione.save(ind);
    		sessione.save(giovanni);
    		
    		sessione.getTransaction().commit();
    		
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			sessione.close();
		}
    	
    }
}
