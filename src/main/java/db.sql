DROP DATABASE IF EXISTS gestione_personale;
CREATE DATABASE gestione_personale;
USE gestione_personale;

CREATE TABLE indirizzo(
	indirizzoID INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	via VARCHAR(250),
    citta VARCHAR(250),
    provincia VARCHAR(50)
);

CREATE TABLE dipendente(
	dipendenteID INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    codice_dip VARCHAR(50) UNIQUE,
    nominativo VARCHAR(250),
    indirizzo_rif INTEGER UNIQUE,
    FOREIGN KEY (indirizzo_rif) REFERENCES indirizzo(indirizzoID)
);

SELECT *
	FROM dipendente
    INNER JOIN indirizzo ON dipendente.indirizzo_rif = indirizzo.indirizzoID;